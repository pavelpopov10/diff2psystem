Diff2PSystem v0.8 by PavelPopov

Exclusive patch-system by 2programm.
# Features #
* directories diff to file
* binary files
* byte by byte comparision
* saving space
* fast in work
* can be used as two-part system in other programms (really simple API)

# Use #
* Make: java -jar -Xms256m -Xmx1024m Diff2PSystem.jar sourcedir targetdir patch.diff2P
* Patch: java -jar Diff2PSystem.jar patch.diff2P todir