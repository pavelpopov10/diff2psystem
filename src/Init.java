
import java.io.File;
import util.Diff2PSystem;
import util.Logger;

public class Init {

	public static final String ver = "0.8.1";

	public static void main(String[] args) throws Exception {
		Logger.write("Initialization Diff2PSystem v" + ver + "...");
		if (args.length == 3) {
			File source = new File(args[0]), target = new File(args[1]), patch = new File(args[2]);
			if (source.isDirectory() && target.isDirectory()) {
				Logger.write("Making diff for " + source.getPath() + " to " + target.getPath() + "...");
				if (!Diff2PSystem.make(source, target, patch)) {
					Logger.write(Logger.LogState.ERROR, "Nothing to change!");
					System.exit(4);
				}
			} else {
				Logger.write(Logger.LogState.ERROR, "Source or/and target not directories!");
				System.exit(2);
			}
		} else if (args.length == 2) {
			File patch = new File(args[0]), target = new File(args[1]);
			if (patch.isFile() && target.isDirectory()) {
				Logger.write("Applying patch " + patch.getPath() + " to " + target.getPath() + "...");
				if (!Diff2PSystem.patch(patch, target)) {
					Logger.write(Logger.LogState.ERROR, "Patching failed!");
					System.exit(4);
				}
			} else {
				Logger.write(Logger.LogState.ERROR, "Patch isn't a file or/and target isn't a directory!");
				System.exit(2);
			}
		} else {
			Logger.write(Logger.LogState.ERROR, "Parameters are incorrect!");
			System.exit(1);
		}
	}
}
