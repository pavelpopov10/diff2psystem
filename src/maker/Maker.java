package maker;

import java.io.*;
import util.Logger;

public class Maker {

	private static int fragments = 1, fsize = 1024 * 1024; // 1Mb
	private static File tempf = new File("temp");
	private static FileOutputStream tempfos;

	public static PatchWriter make(InputStream sis, InputStream tis, PatchWriter w) throws IOException, InterruptedException {
		tempf.delete();
		tempfos = new FileOutputStream(tempf);
		int skipped = 0, wrotten = 0;
		while (true) {
			int sb = sis.read();
			int tb = tis.read();
			if (tb == -1) {
				if (wrotten > 0) {
					tempfos.close();
					tempfos = null;
					w.writeDATA(wrotten);
					write(w, new FileInputStream(tempf), wrotten, wrotten);
				}
				if (skipped > 0) {
					skip(w, skipped);
				}
				break;
			} else if (sb == tb) {
				if (wrotten > 0) {
					tempfos.close();
					tempfos = null;
					w.writeDATA(wrotten);
					write(w, new FileInputStream(tempf), wrotten, wrotten);
					wrotten = 0;
				}
				skipped++;
			} else {
				if (skipped > 0) {
					skipped = skip(w, skipped);
				}
				createTempFile();
				tempfos.write(tb);
				wrotten++;
			}
		}
		sis.close();
		tis.close();
		return w;
	}

	public static void add(FileInputStream fis, long size, PatchWriter w) throws IOException {
		w.writeDATA(size);
		write(w, fis, (int) size, (int) size);
	}

	private static void createTempFile() throws IOException {
		if (tempfos == null) {
			tempf.delete();
			tempf.createNewFile();
			tempf.deleteOnExit();
			tempfos = new FileOutputStream(tempf);
		}
	}

	private static void write(PatchWriter w, FileInputStream tempfis, int size, int startsize) throws IOException {
		int tsize = (size > fsize ? fsize : size);
		byte[] data = new byte[tsize];
		tempfis.read(data);
		w.getDOS().write(data);
		if (size > fsize) {
			Logger.write("[" + fragments + "/" + (startsize / fsize) + "] fragments wrotten");
			fragments++;
			write(w, tempfis, size - fsize, startsize);
		} else {
			Logger.write(startsize + " wrotten");
			fragments = 1;
		}
		tempfis.close();
		tempf.delete();
	}

	private static int skip(PatchWriter w, int skipped) throws IOException {
		Logger.write(skipped + " skipped");
		w.skip(skipped);
		return 0;
	}
}
