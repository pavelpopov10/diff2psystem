package maker;

import java.io.*;

public class PatchWriter
{

	private DataOutputStream dos;
	private static final byte[] HEADER = new byte[]{(byte) 0xBC, (byte) 0xAB, (byte) 0xEF, (byte) 0xCD};
	private static final int EOF = 0xFF,
			DATA_BYTE = 0x01,
			DATA_INT = 0x02,
			DATA_LONG = 0x03,
			SKIP_BYTE = 0x04,
			SKIP_INT = 0x05,
			SKIP_LONG = 0x06,
			FILE_PATH = 0x07;

	public PatchWriter(OutputStream os)
	{
		dos = new DataOutputStream(os);
	}

	/**
	 * Use init before any operations
	 * (It writes a header)
	 *
	 * @throws IOException
	 */
	public void init() throws IOException
	{
		write(HEADER);
	}

	/**
	 * Use path before any change command to split patch files
	 *
	 * @param path filepath
	 *
	 * @throws IOException
	 */
	public void path(String path) throws IOException
	{
		write((byte) FILE_PATH);
		write((int) path.length());
		write(path);
	}

	/**
	 * Use skip to order applyer write n bytes from original file.<br>
	 * It gives us a main opportunity to make patches smaller than all target file.
	 *
	 * @param n number of bytes
	 *
	 * @throws IOException
	 */
	public void skip(long n) throws IOException
	{
		long z = n / 255;
		if (z == 0) {
			dos.write((byte) SKIP_BYTE);
			write((byte) n);
		} else if (z > 0 && z < 4) {
			dos.write((byte) SKIP_INT);
			write((int) n);
		} else {
			dos.write((byte) SKIP_LONG);
			write(n);
		}
	}

	/**
	 * Use finish to correctly close the DataOutputStream and write last EOF byte.
	 *
	 * @throws IOException
	 */
	public void finish() throws IOException
	{
		dos.write((byte) EOF);
		dos.flush();
		dos.close();
	}

	public DataOutputStream getDOS()
	{
		return dos;
	}

	// DataOutputStream func wrappers
	public int size()
	{
		return dos.size();
	}

	public void writeDATA(long length) throws IOException
	{
		long z = length / 255;
		if (z == 0) {
			dos.write((byte) DATA_BYTE);
			write((byte) length);
		} else if (z > 0 && z < 4) {
			dos.write((byte) DATA_INT);
			write((int) length);
		} else {
			dos.write((byte) DATA_LONG);
			write(length);
		}
	}

	public void write(byte[] b) throws IOException
	{
		dos.write(b);
		dos.flush();
	}

	public void write(String s) throws IOException
	{
		dos.writeBytes(s);
		dos.flush();
	}

	public void write(byte b) throws IOException
	{
		dos.writeByte((int) b);
		dos.flush();
	}

	public void write(int i) throws IOException
	{
		dos.writeInt(i);
		dos.flush();
	}

	public void write(long l) throws IOException
	{
		dos.writeLong(l);
		dos.flush();
	}

	public void write(float f) throws IOException
	{
		dos.writeFloat(f);
		dos.flush();
	}

	public void write(boolean b) throws IOException
	{
		dos.writeBoolean(b);
		dos.flush();
	}
}
