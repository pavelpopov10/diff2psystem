package patcher;

import java.io.*;
import java.util.Arrays;

public class PatchReader {

	private DataInputStream dis;
	private static final byte[] HEADER = new byte[]{(byte) 0xBC, (byte) 0xAB, (byte) 0xEF, (byte) 0xCD};
	private static final int EOF = 0xFF,
			DATA_BYTE = 0x01,
			DATA_INT = 0x02,
			DATA_LONG = 0x03,
			SKIP_BYTE = 0x04,
			SKIP_INT = 0x05,
			SKIP_LONG = 0x06,
			FILE_PATH = 0x07;

	public PatchReader(InputStream is) {
		dis = new DataInputStream(is);
	}

	/**
	 * Use init before any operations
	 * (It reads a header)
	 *
	 * @return true, if header is correct
	 *
	 * @throws IOException
	 */
	public boolean init() throws IOException {
		byte[] in = new byte[HEADER.length];
		dis.read(in);
		if (Arrays.equals(HEADER, in)) {
			return true;
		}
		return false;
	}

	public DataInputStream getDIS() throws IOException {
		return dis;
	}

	/**
	 * Checks if byte is a EOF
	 *
	 * @param b byte to check
	 *
	 * @return true if byte is an EOF command
	 */
	public boolean isEOF(int b) {
		if (b == EOF) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if byte is a PATH command
	 *
	 * @param b byte to check
	 *
	 * @return path if byte is a PATH command or null if not
	 */
	public String isPATH(int b) throws IOException {
		if (b == FILE_PATH) {
			int length = dis.readInt();
			if (length <= 0) {
				return null;
			}
			byte[] path = new byte[length];
			dis.read(path);
			return new String(path);
		} else {
			return null;
		}
	}

	public long isDATA(int b) throws IOException {
		switch (b) {
			case DATA_BYTE:
				return ((long) dis.readByte() & 0xFFL);
			case DATA_INT:
				return Long.valueOf(dis.readInt());
			case DATA_LONG:
				return dis.readLong();
			default:
				return 0L;
		}
	}

	/**
	 * Checks if byte is a SKIP command
	 *
	 * @param b byte to check
	 *
	 * @return byte array containing
	 * <ul>
	 * <li>1 byte - if it's a SKIP_BYTE</li>
	 * <li>4 bytes - if it's a SKIP_INT</li>
	 * <li>8 bytes - if it's a SKIP_LONG</li>
	 * <li>0 bytes - if byte is not a SKIP command</li>
	 * </ul>
	 */
	public long isSKIP(int b) throws IOException {
		switch (b) {
			case SKIP_BYTE:
				return ((long) dis.readByte() & 0xFFL);
			case SKIP_INT:
				return Long.valueOf(dis.readInt());
			case SKIP_LONG:
				return dis.readLong();
			default:
				return 0L;
		}
	}
}
