package patcher;

import java.io.*;
import util.Logger;

public class Patcher {

	private static int fragments = 1;
	private static long fsize = 1024 * 1024; // 1Mb

	public static String apply(InputStream sis, PatchReader r, OutputStream tos) throws IOException {
		while (true) {
			int b = r.getDIS().read();
			// Check EOF
			if (b == -1 || r.isEOF(b)) {
				break;
			}
			// Check FILEPATH
			String path = r.isPATH(b);
			if (path != null) {
				sis.close();
				tos.close();
				return path;
			}
			// Check DATA
			long datalength = r.isDATA(b);
			if (datalength <= 0) {
				// Check SKIP
				long skiplength = r.isSKIP(b);
				if (skiplength <= 0) {
					Logger.write("Warning! Unknown byte \"" + Integer.toHexString(b) + "\"!");
					continue;
				} else {
					skip(sis, tos, skiplength, skiplength);
				}
			} else {
				write(r, sis, tos, datalength, datalength);
			}
			tos.flush();
		}
		sis.close();
		tos.close();
		return null;
	}

	private static void skip(InputStream sis, OutputStream tos, long skiplength, long startskiplength) throws IOException {
		byte[] source;
		if (skiplength > fsize) {
			source = new byte[(int) fsize];
			sis.read(source);
			tos.write(source);
			source = null;
			Logger.write("[" + fragments + "/" + (startskiplength / fsize) + "] fragments skipped");
			fragments++;
			skip(sis, tos, skiplength - fsize, startskiplength);
		} else {
			source = new byte[(int) skiplength];
			sis.read(source);
			tos.write(source);
			source = null;
			Logger.write(startskiplength + " skipped");
			fragments = 1;
		}
	}

	private static void write(PatchReader r, InputStream sis, OutputStream tos, long datalength, long startdatalength) throws IOException {
		byte[] patch;
		if (datalength > fsize) {
			patch = new byte[(int) fsize];
			r.getDIS().read(patch);
			tos.write(patch);
			sis.skip(datalength);
			patch = null;
			Logger.write("[" + fragments + "/" + (startdatalength / fsize) + "] fragments wrotten");
			fragments++;
			write(r, sis, tos, datalength - fsize, startdatalength);
		} else {
			patch = new byte[(int) datalength];
			r.getDIS().read(patch);
			tos.write(patch);
			sis.skip(datalength);
			patch = null;
			Logger.write(startdatalength + " wrotten");
			fragments = 1;
		}
	}
}
