package util;

import java.util.Calendar;

/**
 * Class for getting current date and time
 *
 * @author PavelPopov
 */
public class DateAndTime
{

	private static Calendar c;

	private static String timetostr(int time)
	{
		String timestr = "";
		if (time < 10) {
			timestr = "0";
		}
		return timestr + time;
	}

	/**
	 * Return formatted date<br>
	 *
	 * For example, current date is 07.02.2014 03:25:06, then:
	 * <pre>{@link getCurrDate}("HOUR:MINUTE")</pre>
	 * returns "03:25".<br><br>
	 *
	 * <b>Codes:</b><br>
	 * {@code YEAR} - current year (2014)<br>
	 * {@code MONTH} - current month (02)<br>
	 * {@code DAY} - current day (07)<br>
	 * {@code HOUR} - curren hour (03)<br>
	 * {@code MINUTE} - current minute (25)<br>
	 * {@code SECOND} - current second (06)<br>
	 *
	 * @param code code string
	 *
	 * @return string that parsed with current date/time
	 */
	public static String getCurrDate(String code)
	{
		c = Calendar.getInstance();
		code = code.replaceAll("YEAR", timetostr(c.get(Calendar.YEAR)));
		code = code.replaceAll("MONTH", timetostr((int) c.get(Calendar.MONTH) + 1));
		code = code.replaceAll("DAY", timetostr(c.get(Calendar.DAY_OF_MONTH)));
		code = code.replaceAll("HOUR", timetostr(c.get(Calendar.HOUR_OF_DAY)));
		code = code.replaceAll("MINUTE", timetostr(c.get(Calendar.MINUTE)));
		code = code.replaceAll("SECOND", timetostr(c.get(Calendar.SECOND)));
		return code;
	}

	public static void log(String msg)
	{
		c = Calendar.getInstance();
		System.out.println("["
						   + timetostr(c.get(Calendar.DAY_OF_MONTH))
						   + "."
						   + timetostr((int) c.get(Calendar.MONTH) + 1)
						   + "."
						   + timetostr(c.get(Calendar.YEAR))
						   + " "
						   + timetostr(c.get(Calendar.HOUR_OF_DAY))
						   + ":"
						   + timetostr(c.get(Calendar.MINUTE))
						   + ":"
						   + timetostr(c.get(Calendar.SECOND))
						   + "] "
						   + msg);
	}
}
