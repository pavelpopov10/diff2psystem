package util;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.zip.*;
import maker.Maker;
import maker.PatchWriter;
import patcher.PatchReader;
import static patcher.Patcher.apply;

public class Diff2PSystem
{

	public static boolean patch(File diff2P, File sourceDir) throws Exception
	{
		Logger.write("Getting filelists...");

		File sourceDirFull = sourceDir.getCanonicalFile();
		int sourceFullPathLength = sourceDir.getCanonicalPath().length() + 1;

		Collection<String> sourceFiles = getAllFiles(sourceDir.getCanonicalFile());
		Collection<String> doneFiles = new ArrayList<>();
		Collection<String> removeFiles = new ArrayList<>();

		PatchReader r = new PatchReader(new FileInputStream(diff2P));
		if (!r.init()) {
			Logger.write("Patch file isn't correct: Header is incorrect!");
			return false;
		}
		byte b = r.getDIS().readByte();
		String path = r.isPATH(b), pathsp = c2sp(path), nextpath;
		if (path == null) {
			Logger.write("Patch file isn't correct: First patch file path is not set!");
			return false;
		} else {
			Logger.write("PATH is " + path);
			createDirs(sourceDirFull.getCanonicalPath() + File.separator + pathsp);
		}
		while (true) {
			String fullPath = sourceDirFull.getCanonicalPath() + File.separator + pathsp;
			File source = new File(fullPath);
			if (!source.exists()) {
				source.createNewFile();
			}
			File patched = new File(fullPath + ".patched");
			nextpath = apply(new FileInputStream(source), r, new FileOutputStream(patched));
			// Renaming patched file
			source.delete();
			patched.renameTo(source);
			doneFiles.add(fullPath);
			if (nextpath == null) {
				break;
			} else {
				Logger.write("PATH is " + nextpath);
				path = nextpath;
				pathsp = c2sp(path);
				createDirs(sourceDirFull.getCanonicalPath() + File.separator + pathsp);
			}
		}
		removeFiles.addAll(sourceFiles);
		removeFiles.removeAll(doneFiles);
		for (String fullPath : removeFiles) {
			String localPath = fullPath.substring(sourceFullPathLength);
			Logger.write("DELETE " + localPath);
			if (!new File(fullPath).delete()) {
				Logger.write("Can't delete file " + localPath + "!");
			}
		}
		return true;
//		if (sourceFiles.isEmpty()) {
//			for (String deltaFile : deltaFiles) {
//				// ADD
//				Logger.write("ADD " + deltaFile);
//				createTempDirs(sourceDirFull + File.separator + deltaFile);
//				// Moving delta file
//				File delta = new File(deltaFile);
//				delta.renameTo(new File(sourceDirFull + File.separator + deltaFile.split(".delta")[0]));
//			}
//			return true;
//		} else {
//			// Delete (FOR APPLYING!!!)
//			for (String sourceFileFullPath : sourceFiles) {
//				String sourceFileLocalPath = sourceFileFullPath.substring(sourceFullPathLength);
//				if (deltaFiles.contains(sourceFileLocalPath + ".delta") && !doneFiles.contains(sourceFileLocalPath)) {
//					// CHANGE
//					File source = new File(sourceFileFullPath).getCanonicalFile();
//					File delta = new File(sourceFileLocalPath + ".delta").getCanonicalFile();
//					File patched = new File(sourceFileFullPath + ".patched").getCanonicalFile();
//
//					Logger.write("CHANGE " + sourceFileLocalPath);
//					createTempDirs(sourceFileFullPath);
//					Logger.write(String.valueOf(XDeltaEncoder.launch(
//							new String[]{"-d", source.getCanonicalPath(), delta.getPath(), patched.getCanonicalPath()})));
//
//					if (delta.exists()) {
//						Logger.write("Delta exists!");
//					}
//					if (source.exists()) {
//						Logger.write("Source exists!");
//					}
//					if (patched.exists()) {
//						Logger.write("Patched exists!");
//					}
//
//					// Deleting delta file
//					if (!delta.delete()) {
//						Logger.write("Can't delete " + delta.getPath() + " (" + delta.getCanonicalPath() + ")");
//						delta.deleteOnExit();
//					}
//					// Deleting original
//					if (!source.delete()) {
//						Logger.write("Can't delete " + source.getPath() + " (" + source.getCanonicalPath() + ")");
//					}
//					// Renaming patched
//					if (!patched.renameTo(source)) {
//						Logger.write("Can't rename " + patched.getPath() + " (" + patched.getCanonicalPath() + ") to " + source.getPath() + " (" + source.getCanonicalPath() + ")");
//					}
//					doneFiles.add(sourceFileLocalPath);
//				} else if (!deltaFiles.contains(sourceFileLocalPath) && new File(sourceFileFullPath).exists()) {
//					// DELETE
//					Logger.write("DELETE " + sourceFileLocalPath);
//					new File(sourceFileFullPath).delete();
//					doneFiles.add(sourceFileLocalPath);
//				}
//			}
//			for (String deltaFile : deltaFiles) {
//				if (!doneFiles.contains(deltaFile.split(".delta")[0])) {
//					// ADD
//					Logger.write("ADD " + deltaFile.split(".delta")[0]);
//					createTempDirs(sourceDirFull + File.separator + deltaFile);
//					// Moving delta file
//					File delta = new File(deltaFile);
//					delta.renameTo(new File(sourceDirFull + File.separator + deltaFile.split(".delta")[0]));
//				}
//			}
//			return true;
//		}
	}

	public static boolean make(File sourceDir, File targetDir, File diff2P) throws IOException, InterruptedException
	{
		Logger.write("Getting filelists...");

		File sourceDirFull = sourceDir.getCanonicalFile();
		int targetFullPathLength = targetDir.getCanonicalPath().length() + 1;

		Collection<String> sourceFiles = getAllFiles(sourceDir.getCanonicalFile());
		Collection<String> targetFiles = getAllFiles(targetDir.getCanonicalFile());
		Collection<String> doneFiles = new ArrayList<>();

		if (sourceFiles.isEmpty() && targetFiles.isEmpty()) {
			Logger.write("Source and target directory contains no files!");
			return false;
		}

		// Equal check
//		if (targetFiles.equals(sourceFiles)) {
		// CRC32 CHECK
		// IF EQUALS - ABORT
//		}

		PatchWriter w = new PatchWriter(new FileOutputStream(diff2P));
		w.init();
		// Add (FOR PATCHING!!!)
		for (String targetFileFullPath : targetFiles) {
			String targetFileLocalPath = targetFileFullPath.substring(targetFullPathLength);
			String targetFileLocalPathjp = c2jp(targetFileLocalPath);
			if (sourceFiles.contains(sourceDirFull.getCanonicalPath() + File.separator + targetFileLocalPath) && !doneFiles.contains(targetFileLocalPath)) {
				// CHANGE
				Logger.write("CHANGE " + targetFileLocalPathjp);
				w.path(targetFileLocalPath);
				Maker.make(new FileInputStream(new File(sourceDirFull.getCanonicalPath() + File.separator + targetFileLocalPath)),
						   new FileInputStream(new File(targetFileFullPath)), w);
				doneFiles.add(targetFileLocalPath);
			} else if (!sourceFiles.contains(sourceDirFull.getCanonicalPath() + File.separator + targetFileLocalPath) && !doneFiles.contains(targetFileLocalPath)) {
				// ADD
				Logger.write("ADD " + targetFileLocalPathjp);
				// Patch
				w.path(targetFileLocalPathjp);
				File tf = new File(targetFileFullPath);
				Maker.add(new FileInputStream(tf), tf.length(), w);
				doneFiles.add(targetFileLocalPath);
			}
		}
		w.finish();
		return true;
	}

	private static void createDirs(String dir) throws IOException
	{
		File parent = new File(dir).getParentFile();
		if (parent != null && !parent.exists() && parent.mkdirs()) {
			Logger.write("MKDIR " + parent.getPath() + " (" + parent.getCanonicalPath() + ")");
		}
	}

	private static Collection<String> getAllFiles(File dir) throws IOException
	{

		Collection<String> files = new ArrayList<>();
		File[] all = dir.listFiles();
		for (int i = 0; i < all.length; ++i) {
			File ff = all[i];
			if (ff.isDirectory() && !ff.isFile()) {
				Collection<String> inDirFiles = getAllFiles(ff);
				if (inDirFiles == null || inDirFiles.isEmpty()) {
					continue;
				}
				for (String inDirFile : inDirFiles) {
					files.add(inDirFile);
				}
			} else {
				files.add(all[i].getCanonicalPath());
			}
		}
		return files;
	}

	private static String c2jp(String t)
	{
		return t.replace('\\', '/');
	}

	private static String c2sp(String t)
	{
		return t.replace('/', File.separatorChar);
	}
}
