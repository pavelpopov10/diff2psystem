package util;

/**
 * Logger class for writing logs
 *
 * @author PavelPopov
 */
public class Logger
{

	/**
	 * LogState sets the log states<br>
	 *
	 * {@code INFO} is for standart logs<br>
	 * {@code WARNING} is for system warnings<br>
	 * {@code ERROR} is for errors and exceptions<br>
	 */
	public enum LogState
	{

		INFO,
		WARNING,
		ERROR,
	}

	/**
	 * Writes exception
	 *
	 * @param ex exception
	 */
	public static void write(Exception ex)
	{
		write(LogState.ERROR, ex.getMessage());
	}

	/**
	 * Writes info log
	 *
	 * @param log logging string
	 */
	public static void write(String log)
	{
		write(LogState.INFO, log);
	}

	/**
	 * Writes log with LogState
	 *
	 * @param s   {@link LogState} of log
	 * @param log logging string
	 */
	public static void write(LogState s, String log)
	{
		switch (s) {
			case INFO:
				System.out.println(DateAndTime.getCurrDate("[DAY.MONTH.YEAR HOUR:MINUTE:SECOND]") + " " + log);
				break;
			case WARNING:
				System.err.println(DateAndTime.getCurrDate("[DAY.MONTH.YEAR HOUR:MINUTE:SECOND]") + " [WARNING] " + log);
				break;
			case ERROR:
				System.err.println(DateAndTime.getCurrDate("[DAY.MONTH.YEAR HOUR:MINUTE:SECOND]") + " [ERROR] " + log);
				break;
		}
	}
}
